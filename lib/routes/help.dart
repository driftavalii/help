import 'package:flutter/material.dart';
import '../screens/questionScreen.dart';

class Help extends StatelessWidget {
  // TODO: add state variables, methods and constructor params
  Help();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //debugShowCheckedModeBanner: false,
      title: 'Help',
      home: QuestionScreen(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/home',
      onGenerateRoute: _getRoute,
    );
  }

  Route<dynamic> _getRoute(RouteSettings settings) {
    if (settings.name != '/home') {
      return null;
    }

    return MaterialPageRoute<void>(
      settings: settings,
      builder: (BuildContext contex) => QuestionScreen(),
      fullscreenDialog: true,
    );
  }
}

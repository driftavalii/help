// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quiz.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Quiz _$QuizFromJson(Map<String, dynamic> json) {
  return new Quiz((json['questions'] as List)
      ?.map((e) =>
          e == null ? null : new Question.fromJson(e as Map<String, dynamic>))
      ?.toList());
}

abstract class _$QuizSerializerMixin {
  List<Question> get questions;
  Map<String, dynamic> toJson() => <String, dynamic>{'questions': questions};
}

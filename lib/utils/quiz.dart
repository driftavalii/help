import 'package:json_annotation/json_annotation.dart';
import './question.dart';

part 'quiz.g.dart';

@JsonSerializable()

class Quiz extends Object with _$QuizSerializerMixin {
  List<Question> questions;
  int _currentQuestionIndex = -1;
  int _score = 0;

  Quiz(this.questions) {
    questions.shuffle();
  }

  int get questionNumber => _currentQuestionIndex + 1;
  int get length => questions.length;

  Question get nextQuestion {
    _currentQuestionIndex++;
    if (_currentQuestionIndex >= length) return null;
    return questions[_currentQuestionIndex];
  }

  factory Quiz.fromJson(Map<String, dynamic> json) => _$QuizFromJson(json);
}
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Question _$QuestionFromJson(Map<String, dynamic> json) {
  return new Question(
      question: json['question'] as String,
      option1: json['option1'] as String,
      option2: json['option2'] as String,
      option3: json['option3'] as String,
      option4: json['option4'] as String,
      answer: json['answer'] as String);
}

abstract class _$QuestionSerializerMixin {
  String get question;
  String get option1;
  String get option2;
  String get option3;
  String get option4;
  String get answer;
  Map<String, dynamic> toJson() => <String, dynamic>{
        'question': question,
        'option1': option1,
        'option2': option2,
        'option3': option3,
        'option4': option4,
        'answer': answer
      };
}

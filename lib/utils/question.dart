import 'package:json_annotation/json_annotation.dart';

part 'question.g.dart';

@JsonSerializable()

class Question extends Object with _$QuestionSerializerMixin {
  final String question;
  final String option1;
  final String option2;
  final String option3;
  final String option4;
  final String answer;

  Question(
      {this.question,
      this.option1,
      this.option2,
      this.option3,
      this.option4,
      this.answer});

  factory Question.fromJson(Map<String, dynamic> json) =>
      _$QuestionFromJson(json);
}


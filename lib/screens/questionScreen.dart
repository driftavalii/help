import 'dart:convert';
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../utils/quiz.dart';
import '../utils/question.dart';

Future<Quiz> fetchQuiz() async {
  final response = await http.get('www.cloudace.io/questions');

  if (response.statusCode == 200) {
    return Quiz.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load quiz');
  }
}

List<Question> parseQuestions(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Question>((json) => Question.fromJson(json)).toList();
}

Future<List<Question>> fetchQuestion(http.Client client) async {
  final response = await client.get('http://www.cloudace.io/questions');

  return compute(parseQuestions, response.body);
}

class QuestionScreen extends StatefulWidget {
  @override
  _QuestionScreenState createState() => new _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  Quiz quiz;
  Question currentQuestion;
  String questionText;
  int questionNumber;
  bool isCorrect;
  String option1;
  String option2;
  String option3;
  String option4;
  String answer;
  bool _visible = true;
  int index;
  int radioValue = 0;
  int counter = 0;

  void handleRadioValueChanged(int value) {
    setState(() {
      radioValue = value;
    });
  }

  handleNextButton(int quizLength) {
    int start = 1;
      if (start < quizLength) {
        counter = start;
        start++;        
      } else {
        print('The quiz is still running'); //handleQuizEnd();
      }
      radioValue = 0;
      /**setState(() {
      int start = 1;
      if (start < quizLength) {
        counter = start;
        start++;        
      } else {
        print('The quiz is still running'); //handleQuizEnd();
      }
      radioValue = 0;
    });**/
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('AWS Advanced Networking'),
      ),
      body: FutureBuilder<List<Question>>(
        future: fetchQuestion(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            index = snapshot.data.length;
            quiz = Quiz(snapshot.data);
            return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 40.0),
                      child: Text(quiz.questions[counter]
                          .question), //child: Text(snapshot.data[0].question),
                    ),
                  ),
                  Divider(),
                  Expanded(
                    flex: 4,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Radio<int>(
                                value: 1,
                                groupValue: radioValue,
                                onChanged: handleRadioValueChanged,
                              ),
                              Expanded(
                                child: Text(quiz.questions[counter]
                                    .option1), // child: Text(snapshot.data[0].option1),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Radio<int>(
                                value: 2,
                                groupValue: radioValue,
                                onChanged: handleRadioValueChanged,
                              ),
                              Expanded(
                                child: Text(quiz.questions[counter]
                                    .option2), //child: Text(snapshot.data[0].option2),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Radio<int>(
                                value: 3,
                                groupValue: radioValue,
                                onChanged: handleRadioValueChanged,
                              ),
                              Expanded(
                                child: Text(quiz.questions[counter]
                                    .option3), //child: Text(snapshot.data[0].option3),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              Radio<int>(
                                value: 4,
                                groupValue: radioValue,
                                onChanged: handleRadioValueChanged,
                              ),
                              Expanded(
                                child: Text(quiz.questions[counter]
                                    .option4), //child: Text(snapshot.data[0].option4),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Divider(),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              padding: EdgeInsets.all(8.0),
                              child: RaisedButton(
                                child: Text('Next'),
                                onPressed: handleNextButton(index),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
